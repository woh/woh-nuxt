module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended',
    'airbnb-base/legacy'
  ],
  // add your custom rules here
  'rules': {
    "max-len": "off",
    "vue/no-side-effects-in-computed-properties": "off",
    "vue/max-attributes-per-line": "off",
    "vue/require-prop-types": "off",
    "vue/no-v-html": "off",
    "no-console": "off",
    "no-multiple-empty-lines": "off",
    // don't require .vue extension when importing
    'import/extensions': ['error', 'always', {
      'js': 'never',
      'vue': 'never'
    }],
    // allow optionalDependencies
    'import/no-extraneous-dependencies': ['error', {
      'optionalDependencies': ['test/unit/index.js']
    }],
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    // allow import with script-loader in main.js
    'import/no-webpack-loader-syntax': 0
  },
}
