FROM keymetrics/pm2:10-alpine

RUN apk add python2 build-base

RUN mkdir /app
WORKDIR /app
COPY . /app/

RUN npm ci
RUN npm run build

CMD ["pm2-runtime", "start", "ecosystem.config.js"]
