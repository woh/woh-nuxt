// eslint-disable-next-line import/prefer-default-export
export const configFroala = {
  data() {
    return {
      config: {
        key: 'dKA5cC2A2B1H2D2A4zpgaG5yyqB2C1E7C7E1F5F4A1B3A6==',
        language: 'ru',
        placeholderText: 'Это будет великолепно!',
        imageDefaultAlign: 'center',
        imageDefaultWidth: 0,
        htmlDoNotWrapTags: ['script', 'style', 'img'],
        imageUploadRemoteUrls: false,
        toolbarBottom: true,
        charCounterCount: false,
        quickInsertEnabled: false,
        linkAlwaysBlank: true,
        paragraphFormat: {
          N: 'Normal',
          H2: 'Heading 1',
          H3: 'Heading 2'
        },
        linkInsertButtons: ['linkBack'],
        imageInsertButtons: ['imageBack', '|', 'imageUpload', 'imageByURL'],
        videoInsertButtons: ['videoBack', '|', 'videoByURL', 'videoEmbed'],
        toolbarButtons: {
          moreText: {
            buttons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'textColor', 'clearFormatting', 'backgroundColor']
          },
          moreParagraph: {
            buttons: ['alignLeft', 'alignCenter', 'formatOLSimple', 'alignRight', 'alignJustify', 'formatOL', 'formatUL', 'paragraphFormat', 'lineHeight', 'outdent', 'indent', 'quote']
          },
          moreRich: {
            buttons: ['insertLink', 'insertImage', 'insertVideo', 'insertTable', 'emoticons', 'fontAwesome', 'specialCharacters', 'insertHR']
          },
          moreMisc: {
            buttons: ['undo', 'redo', 'spellChecker', 'selectAll', 'html', 'help', 'clear', 'fullscreen'],
            align: 'right',
            buttonsVisible: 2
          }
        }
      }
    };
  }
};

