module.exports = {
  apps: [{
    name: 'WoH Front',
    script: 'npx nuxt start',
    instances: 1,
    autorestart: false,
    watch: false,
    max_memory_restart: '1G'
  }]
};
