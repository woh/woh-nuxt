import webpack from 'webpack';
export default {
  server: {
    port: 3000,
    host: '0.0.0.0'
  },
  mode: 'universal',
  render: {
    bundleRenderer: {
      runInNewContext: 'once'
    }
  },
  env: {
    apiUrl: '',
    imgRoot: 'http://dev.woh.ru',
    imgProxy: 'http://dev.woh.ru:3003/'
  },
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, user-scalable=no' },
      { name: 'yandex-verification', content: 'aca924de9211af68' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=cyrillic' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:400,500&display=swap&subset=cyrillic' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,regular,italic,600,600italic,700,700italic,800,800italic&subset=cyrillic' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#29a3da' },
  /*
  ** Global CSS
  */
  css: [
    'element-ui/lib/theme-chalk/index.css',
    'element-ui/lib/theme-chalk/display.css',
    'element-ui/lib/theme-chalk/base.css',
    'swiper/css/swiper.css',
    '~assets/css/foundation.css',
    '~assets/css/hamburgers.css',
    '~assets/css/custom-theme.css',
    '~assets/css/all.css',
    '~assets/css/normalize.css',
    '~assets/css/wowhead.css',
    '~assets/css/woh.css'
  ],
  script: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~plugins/plugins-client', mode: 'client' },
    { src: '~plugins/plugins-server', mode: 'server' },
    { src: '~plugins/filters' }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/router',
    ['@nuxtjs/google-analytics', {
      id: 'UA-40967981-2'
    }]
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    ['@nuxtjs/dotenv', { systemvars: true }],
    '@nuxtjs/sentry',
    '@nuxtjs/pwa',
    ['@nuxtjs/axios', {
      baseURL: 'http://dev.woh.ru/api'
    }
    ],
    'cookie-universal-nuxt',
    [
      '@nuxtjs/yandex-metrika',
      {
        id: '48160019',
        webvisor: true,
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true
      }
    ],
    ['@nuxtjs/component-cache', {
      max: 10000,
      maxAge: 1000 * 60 * 60
    }]
  ],
  /*
  ** Build configuration
  */
  build: {
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
      })
    ],
    extend(config, ctx) {
    }
  },
  sentry: {
    dsn: '',
    disabled: false,
    publishRelease: false
  }
};
