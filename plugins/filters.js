import Vue from 'vue';
import linkify from 'vue-linkify';
import moment from 'moment';

Vue.directive('linkified', linkify);
Vue.directive('focus', {
  inserted(el) {
    el.focus();
  }
});

moment.locale('ru');
Vue.filter('date', (value) => {
  if (!value) return value;
  return moment(String(value)).format('L LTS');
});

Vue.filter('dateShort', (value) => {
  if (!value) return value;
  return moment(String(value)).format('L');
});

Vue.filter('formatDate', (value) => {
  if (!value) return value;
  return moment(String(value)).calendar();
});
