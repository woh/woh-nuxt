import Vue from 'vue';
import ElementUI from 'element-ui';
import VueFroala from 'vue-froala-wysiwyg';
import 'froala-editor/js/froala_editor.pkgd.min';
import 'froala-editor/js/languages/ru';
import 'froala-editor/js/plugins.pkgd.min';
import lang from 'element-ui/lib/locale/lang/ru-RU';
import locale from 'element-ui/lib/locale';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import VueHeadful from 'vue-headful';
import Avatar from 'vue-avatar';
import Croppa from 'vue-croppa';
import Loading from '../components/parts/misc/Loading.vue';
import '../assets/js/auto-hiding';
// import '../assets/js/fitie';

import 'vue-croppa/dist/vue-croppa.css';

// Require Froala Editor js file.
require('froala-editor/js/froala_editor.pkgd.min.js');
// Require Froala Editor css files.
require('froala-editor/css/froala_editor.pkgd.min.css');
require('froala-editor/css/froala_style.min.css');


Vue.use(VueAwesomeSwiper);
Vue.use(ElementUI);
Vue.use(Croppa);
Vue.use(VueFroala);
locale.use(lang);
Vue.component('loading', Loading);
Vue.component('vue-headful', VueHeadful);
Vue.component('Avatar', Avatar);
