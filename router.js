import Vue from 'vue';
import Router from 'vue-router';

import PostsList from './components/posts/PostsList.vue';
import PostsByTag from './components/posts/PostsByTag.vue';
import PostsByCategory from './components/posts/PostsByCategory.vue';
import PostDetails from './components/posts/PostDetails.vue';
import User from './components/user-profile/User.vue';
import UserNewPost from './components/user-profile/UserNewPost.vue';
import UserPostsList from './components/user-profile/UserPostsList.vue';
import UserSettings from './components/user-profile/UserSettings.vue';
import NotFoundComponent from './components/NotFoundComponent.vue';

Vue.use(Router);

export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      {
        name: 'posts-list', path: '/', component: PostsList
      },
      {
        path: '/user',
        component: User,
        meta: { menu: 'side-bar-user-page' },
        children: [
          {
            name: 'user-posts-list', path: '', component: UserPostsList, meta: { menu: 'side-bar-user-page' }
          },
          {
            name: 'user-new-post', path: '/user-new-post', component: UserNewPost, meta: { menu: 'side-bar-user-page' }
          },
          {
            name: 'user-settings', path: '/user-settings', component: UserSettings, meta: { menu: 'side-bar-user-page' }
          }
        ]
      },
      {
        name: 'posts-by-tag', path: '/tag/:tag', component: PostsByTag
      },
      {
        name: 'posts-by-category-sub', path: '/category-list/:category/:sub', component: PostsByCategory
      },
      {
        name: 'posts-by-category', path: '/category-list/:category', component: PostsByCategory
      },
      {
        name: 'post-details', path: '/category-list/:category/post/:id', component: PostDetails
      },
      { path: '*', component: NotFoundComponent }
    ],
    scrollBehavior(to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition;
      }
      return { x: 0, y: 0 };
    }
  });
}
