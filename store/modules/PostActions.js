/* eslint-disable no-shadow,no-param-reassign,no-unused-vars,no-console */
import {
  YOUTUBE_GET,
  POST_DISLIKE,
  POST_LIKE
} from '../actions/PostActions';
import { POST_SUCCESS } from '../actions/PostGet';

const state = { post: {}, youtube: {} };

const getters = {
  youtube: state => state.youtube
};

const actions = {
  [POST_LIKE]: function postLike({ commit, dispatch }, id) {
    return new Promise((resolve, reject) => {
      this.$axios({ url: `/${id}/like/`, method: 'POST' })
        .then((response) => {
          commit(POST_SUCCESS, response);
          resolve(response);
        })
        .catch((err) => {
          this.$sentry.captureException(err);
          reject(err);
        });
    });
  },
  [POST_DISLIKE]: function postDisLike({ commit, dispatch }, id) {
    return new Promise((resolve, reject) => {
      this.$axios({ url: `/${id}/dislike/`, method: 'POST' })
        .then((response) => {
          commit(POST_SUCCESS, response);
          resolve(response);
        })
        .catch((err) => {
          this.$sentry.captureException(err);
          reject(err);
        });
    });
  },
  [YOUTUBE_GET]: function youtubeGet({ commit }, id) {
    this.$axios({ url: `/youtube/${id}` })
      .then((response) => {
        commit(YOUTUBE_GET, response);
      })
      .catch((err) => {
        this.$sentry.captureException(err);
        console.log(err);
      });
  }
};

const mutations = {
  [YOUTUBE_GET]: (state, response) => {
    state.youtube = response.data;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
