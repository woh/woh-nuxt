/* eslint-disable no-shadow,no-param-reassign,no-console */

import { TEASERS_REQUEST, TEASERS_SUCCESS } from '../actions/Teaser';

const state = {
  teasers: {
    teasersDone: false,
    teasersAll: [],
    teasers: [],
    featured: [],
    teaserView: {},
    featuredView: []
  }
};

const getters = {
  teasers: state => state.teasers,
  teasersDone: state => !!state.teasers.teasersDone
};

const actions = {
  [TEASERS_REQUEST]: async function teasersRequest({ commit }) {
    commit(TEASERS_REQUEST);
    await this.$axios({ url: '/teasers/', method: 'GET' })
      .then((response) => {
        commit(TEASERS_SUCCESS, response);
      })
      .catch((err) => {
        this.$sentry.captureException(err);
      });
  }
};

const mutations = {
  [TEASERS_REQUEST]: (state) => {
    state.teasers.teasersDone = false;
  },

  [TEASERS_SUCCESS]: (state, response) => {
    state.teasers.teasersAll = [];
    if (response.data) {
      state.teasers.teasersAll = response.data;
    }

    state.teasers.teasers = state.teasers.teasersAll.filter(teaser => teaser.type === 'TYPE_TEASER');
    state.teasers.featured = state.teasers.teasersAll.filter(teaser => teaser.type === 'TYPE_FEATURE');
    state.teasers.teaserView = state.teasers.teasers ? state.teasers.teasers[0] : undefined;
    state.teasers.featuredView = state.teasers.featured.filter((featured, index) => index < 2);
    state.teasers.teasersDone = true;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
