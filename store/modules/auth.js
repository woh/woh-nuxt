/* eslint-disable promise/param-names,no-shadow,no-param-reassign,
no-unused-vars,no-console,prefer-destructuring, no-new */
import {
  AUTH_REQUEST, AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT, AUTH_REGISTRATION, AUTH_REG_DONE, AUTH_REG_ERROR, AUTH_SHOW
} from '../actions/auth';
import { USER_REQUEST } from '../actions/user';
import user from './user';

const state = {
  token: '',
  status: '',
  regStatus: '',
  authorizeShow: false
};

const getters = {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status,
  regStatus: state => state.regStatus,
  authorizeShow: state => state.authorizeShow
};

const actions = {
  nuxtServerInit({ dispatch }) {
    const token = this.$cookies.get('user-token') || '';
    state.token = token;
    if (token) {
      this.$axios.defaults.headers.common.Authorization = `Bearer ${token}`;
      dispatch(USER_REQUEST);
    } else {
      user.state.user = {};
    }
  },
  [AUTH_REQUEST]: function authRequest({ commit, dispatch }, user) {
    return new Promise((resolve, reject) => { // The Promise used for router redirect in authorize
      commit(AUTH_REQUEST);
      this.$axios({ url: '/user/login', data: user, method: 'POST' })
        .then((resp) => {
          this.$cookies.set('user-token', resp.data.token, {
            path: '/',
            sameSite: true,
            maxAge: 60 * 60 * 24 * 365
            // httpOnly: true
            // secure: true
          });
          this.$axios.defaults.headers.common.Authorization = `Bearer ${resp.data.token}`;
          commit(AUTH_SUCCESS, resp.data.token);
          dispatch(USER_REQUEST);
          resolve(resp);
        })
        .catch((err) => {
          this.$sentry.captureException(err);
          commit(AUTH_ERROR, err);
          reject(err);
        });
    });
  },

  [AUTH_REGISTRATION]: function authRegistration({ commit, dispatch }, user) {
    return new Promise((resolve, reject) => {
      this.$axios({ url: '/user/register/', data: user, method: 'POST' })
        .then((response) => {
          if (response.status === 201) {
            commit(AUTH_REG_DONE);
            dispatch(AUTH_REQUEST, user);
          }
          resolve(response);
        })
        .catch((err) => {
          this.$sentry.captureException(err);
          if (err.response.status === 409 || err.response.status === 500) {
            commit(AUTH_REG_ERROR);
          }
          reject(err);
        });
    });
  },

  [AUTH_LOGOUT]: function authLogout({ commit, dispatch }) {
    return new Promise((resolve, reject) => {
      commit(AUTH_LOGOUT);
      delete this.$axios.defaults.headers.common.Authorization;
      this.$cookies.remove('user-token');
      resolve();
    });
  }
};

const mutations = {
  [AUTH_REQUEST]: (state) => {
    state.status = 'loading';
  },
  [AUTH_SUCCESS]: (state, token) => {
    state.status = 'success';
    state.token = token;
  },
  [AUTH_ERROR]: (state) => {
    state.status = 'error';
  },
  [AUTH_LOGOUT]: (state) => {
    state.token = '';
    state.status = 'logout';
  },
  [AUTH_REG_DONE]: (state) => {
    state.regStatus = 'success';
  },
  [AUTH_REG_ERROR]: (state) => {
    state.regStatus = 'conflict';
  },
  [AUTH_SHOW]: (state) => {
    state.authorizeShow = state.authorizeShow === false;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
