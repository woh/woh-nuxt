/* eslint-disable no-shadow,no-param-reassign,no-unused-vars,no-console */
import {
  USER_REQUEST, USER_ERROR, USER_SUCCESS,
  AVATAR_SAVE, AVATAR_DEL, USER_SAVE, USER_SAVE_PASS
} from '../actions/user';
import { AUTH_LOGOUT } from '../actions/auth';

const state = { status: '', user: {}, userAvatar: '' };

const getters = {
  user: state => state.user,
  isProfileLoaded: state => !!state.user.email,
  userAvatar: state => state.userAvatar
};

const actions = {
  [USER_REQUEST]: async function userRequest({ commit, dispatch }) {
    commit(USER_REQUEST);
    await this.$axios({ url: '/user', method: 'GET' })
      .then((response) => {
        commit(USER_SUCCESS, response);
      })
      .catch((err) => {
        this.$sentry.captureException(err);
        commit(USER_ERROR);
        // if resp is unauthorized, logout, to
        commit(AUTH_LOGOUT);
        dispatch(AUTH_LOGOUT);
      });
  },
  [AVATAR_SAVE]: function avatarSave({ commit, dispatch }, avatar) {
    return new Promise((resolve, reject) => {
      this.$axios({ url: '/user/avatar/', data: avatar, method: 'POST' })
        .then((response) => {
          commit(AVATAR_SAVE, response);
          resolve(response);
        })
        .catch((err) => {
          this.$sentry.captureException(err);
          reject(err);
        });
    });
  },

  [AVATAR_DEL]: function avatarDel({ commit, dispatch }) {
    this.$axios({ url: '/user/avatar/drop/', method: 'POST' })
      .then((resp) => {
        dispatch(USER_REQUEST);
      })
      .catch((err) => {
        this.$sentry.captureException(err);
      });
  },

  [USER_SAVE]: function userSave({ commit, dispatch }, user) {
    this.$axios({ url: '/user/save/', data: user, method: 'POST' })
      .then((response) => {
        commit(USER_SUCCESS, response);
      })
      .catch((err) => {
        this.$sentry.captureException(err);
      });
  },
  [USER_SAVE_PASS]: function userSavePass({ commit, dispatch }, pass) {
    this.$axios({ url: `${process.env.apiUrl}/user/password`, data: pass, method: 'POST' })
      .then((response) => {
        commit(USER_SAVE_PASS, response);
      })
      .catch((err) => {
        this.$sentry.captureException(err);
      });
  }
};

const mutations = {
  [USER_REQUEST]: (state) => {
    state.status = 'loading';
  },
  [USER_SUCCESS]: (state, response) => {
    const user = response.data;
    if (user.avatar) {
      user.avatar = `${process.env.imgRoot}${user.avatar}`;
    }
    user.isMailConfirmed = user.isMailConfirmed || false;
    user.access = user.role === 'ROLE_ADMIN' || user.role === 'ROLE_MODER';
    state.user = user;
    state.status = 'success';
  },
  [USER_ERROR]: (state) => {
    state.status = 'error';
  },
  [AUTH_LOGOUT]: (state) => {
    state.user = {};
  },
  [AVATAR_SAVE]: (state, response) => {
    state.user.avatar = `${process.env.imgRoot}${response.data}`;
  },
  [USER_SAVE]: (state, response) => {
  },
  [USER_SAVE_PASS]: (state, response) => {
    console.log(response);
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
